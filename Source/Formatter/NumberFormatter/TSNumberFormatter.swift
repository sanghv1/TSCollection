//
//  TSNumberFormatter.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSNumberFormatter Class

*/

public let currencyNumberFormatter = TSNumberFormatter.shared.currencyNumberFormatter

open class TSNumberFormatter {

    open static let shared = TSNumberFormatter()

    private init(){}

    lazy open var currencyNumberFormatter: NumberFormatter = {
        let currencyNumberFormatter = NumberFormatter()

        currencyNumberFormatter.numberStyle = .currency
        currencyNumberFormatter.maximumFractionDigits = 2

        return currencyNumberFormatter
    }()
}
