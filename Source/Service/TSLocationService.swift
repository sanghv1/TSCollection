//
//  TSLocationService.swift
//  TSCollection
//
//  Created by Jimmy on 9/24/16.
//  Copyright © 2016 TS. All rights reserved.
//

import UIKit
import CoreLocation

public class TSLocationService: NSObject {
    
    public static let share = TSLocationService()
    
    public var locationManager : CLLocationManager!
    public var currentLocation : CLLocation?
    public var zipcode: String = TSConstants.EMPTY_STRING
    
    override init() {
        super.init()
        locationManager = CLLocationManager()
    }
    
    public func registerLocation() {
        // Ask for Authorisation from the User.
        locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
}

// MARK: - Get/Set

public extension TSLocationService {
    
    public func getCurrentLocation() -> String {
        if let currentLocation = currentLocation {
            return String(currentLocation.coordinate.longitude) + "-" + String(currentLocation.coordinate.latitude)
        } else {
            return "N/A"
        }
    }
    
    public func updateZipcodeLocation() {
        guard let currentLocation = currentLocation else {
            return
        }

        CLGeocoder().reverseGeocodeLocation(currentLocation, completionHandler: {(placemarks, error)-> Void in
            if error != nil {    
                return
            }
            
            if let placemarks = placemarks {
                if placemarks.count > 0 {
                    let placemark = placemarks[0]
                    if let zipecodeStr = placemark.postalCode {
                        self.zipcode = zipecodeStr
                        NSLog("Zipcode: \(self.zipcode)")

                    } else {
                        self.zipcode = "1234"
                        NSLog("Not get zipcode")
                    }
                } else {
                    
                }
            }
            self.locationManager.stopUpdatingLocation()
        })
    }
}

// MARK: - CLLocationManagerDelegate

extension TSLocationService: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = manager.location
        if currentLocation != nil {
            self.updateZipcodeLocation()
        }
    }
}

