//
//  TSCellDatasource.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSCellDatasource protocol

*/
public protocol TSCellDatasource {

    static var cellIdentifier: String { get }
    func configCellWithData(data: Any?)
}
