//
//  String+TSValidation.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSValidation Extends String

*/
public extension String {
    

}

public extension String {
    /**
     :name:	lines
     */
    public var lines: [String] {
        return components(separatedBy: CharacterSet.newlines)
    }

    /**
     :name:	firstLine
     */
    public var firstLine: String? {
        return lines.first?.trim()
    }

    /**
     :name:	lastLine
     */
    public var lastLine: String? {
        return lines.last?.trim()
    }

    /**
     :name:	replaceNewLineCharater
     */
    public func replaceNewLineCharater(separator: String = " ") -> String {
        return components(separatedBy: CharacterSet.whitespaces).joined(separator: separator).trim()
    }

    /**
     :name:	replacePunctuationCharacters
     */
    public func replacePunctuationCharacters(separator: String = "") -> String {
        return components(separatedBy: CharacterSet.punctuationCharacters).joined(separator: separator).trim()
    }

    /**
     :name:	trim
     */
    public func trim() -> String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}
