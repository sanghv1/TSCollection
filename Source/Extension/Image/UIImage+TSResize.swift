//
//  UIImage+TSResize.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSResize Extends UIImage

*/
public extension UIImage {
    
    func cropImageWithRect(rect: CGRect) -> UIImage {
        let scale: CGFloat = UIScreen.main.scale

        // constrain crop rect to legitimate bounds
        var outputImage = self
        var newRect = rect

        if (newRect.origin.x >= self.size.width || newRect.origin.y >= self.size.height) {
            return outputImage
        }

        if (newRect.origin.x + newRect.size.width >= self.size.width) {
            newRect.size.width = self.size.width - newRect.origin.x
        }

        if (newRect.origin.y + newRect.size.height >= self.size.height) {
            newRect.size.height = self.size.height - newRect.origin.y
        }

        newRect.origin.y = newRect.origin.y * scale
        newRect.size.width = newRect.size.width * scale
        newRect.size.height = newRect.size.height * scale

        // Crop
        let imageRef = self.cgImage!.cropping(to: newRect)
        if let imageRef = imageRef {
            outputImage = UIImage(cgImage:imageRef)
        }

        return outputImage
    }
}
