//
//  UIViewController+TSRotationExtension.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSRotationExtension Extends UIViewController

*/
public extension UIViewController {
    
    // MARK: - Force view controller rotate to specific orientation

    func forceRotateTo(orientation: UIInterfaceOrientation) {
        let value = orientation.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
    }
}
