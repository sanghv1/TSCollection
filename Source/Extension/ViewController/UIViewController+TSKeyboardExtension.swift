//
//  UIViewController+TSKeyboardExtension.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSKeyboardExtension Extends UIViewController

*/
extension UIViewController {
    
    open func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardWillShow(notifcation:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardWillHide(notifcation:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    open func destroyForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    open func keyboardRectFromNotification(notifcation: NSNotification) -> (begin: CGRect, end: CGRect) {
        let keyboardBeginRect = (notifcation.userInfo?[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardEndRect = (notifcation.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        return (keyboardBeginRect, keyboardEndRect)
    }

    @objc open func keyboardWillShow(notifcation: NSNotification) {

    }

    @objc open func keyboardWillHide(notifcation: NSNotification) {
        
    }
}
